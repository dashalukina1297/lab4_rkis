package com.controller;

import com.models.Furniture;
import com.repo.FurnitureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

@Controller
public class FurnitureController {
    @Autowired
    private static  FurnitureRepository furnitureRepository;

    public FurnitureController(FurnitureRepository furnitureRepository) {
        this.furnitureRepository = furnitureRepository;
    }

    @GetMapping("/")
    public String homePage(){
        return "./Home";
    }
    @GetMapping("/all")
    public String allFurniture(Model model){
        List<Furniture> furnitureOptional = furnitureRepository.findAll();
        model.addAttribute("furniture", furnitureOptional);
        return "./allFurniture";
    }

    @GetMapping("/add")
    public  String addFurniture(Model model){
        model.addAttribute("furniture", new Furniture());
        return "/addFurniture";
    }

    @PostMapping("/addFurniturePost")
    public String addFurniturePost(@ModelAttribute("furniture") Furniture furniture) throws UnsupportedEncodingException {
        furnitureRepository.save(furniture);
        return "redirect:/";
    }
   @GetMapping("/{id}/edit")
    public String edit(@PathVariable("id") Long id, Model model){
       Optional<Furniture> furnitureOptional = furnitureRepository.findById(id);
       if(furnitureRepository.existsById(id)){
           model.addAttribute(furnitureOptional.get());
           return "./editFurniture";
       }
       else {
           return "redirect:/";
       }
   }
   @PatchMapping("/editFurniture")
    public String patchFurniture(@ModelAttribute("furniture") Furniture furniture) throws UnsupportedEncodingException{
        System.out.println(furnitureRepository.getById(furniture.getId()));
        furnitureRepository.save(furniture);
        return "redirect:/";
   }




}
