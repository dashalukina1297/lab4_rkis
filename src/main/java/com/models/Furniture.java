package com.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
@Table(name = "furniture")
public class Furniture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String country;
    private String manufacturer;
    private Integer price;
    private Integer year;

    public Furniture(String name, String country, String manufacturer, Integer price, Integer year) {
        this.name = name;
        this.country = country;
        this.manufacturer = manufacturer;
        this.price = price;
        this.year = year;
    }
    @Override
    public String toString(){
        return (getId()+") "+getName() +" " +getCountry() + " "+getManufacturer() +" " +getYear() + "г. " + getPrice() +"₽");
    }
}
